#ifndef PLANSZARUCH_H
#define PLANSZARUCH_H
#include <iostream>
#include "WarunkiKonca.h"

using namespace std;

struct SGracz
{

    char znak;
    string imie;
};
//int ile;

void Rysuj(char **t, int rozmiar);
void Ruch (char **t, int rozmiar, SGracz *gracz);
int minimax(int rozmiar, char **t, SGracz gracz1, SGracz gracz2,SGracz *aktGracz, int poziom);
#endif // PLANSZARUCH_H
