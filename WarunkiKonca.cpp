#include <iostream>
#include "WarunkiKonca.h"

using namespace std;

bool Remis(char **t, int rozmiar)
{
    for (int i=0;i<rozmiar;i++)
        for (int j=0;j<rozmiar;j++)
            if (t[i][j]==' ')
                return false; // jeszcze są wolne miejsca
    cout << "\n!!! REMIS - kazdy wygrywa!!!\n\n";
    return true;
}

//================================================================
bool Wygrana(char **t, SGracz gracz, int rozmiar, bool opis)
{
    int k;
    int ile;
    bool czyWygrana;
    bool czyWyg1;
    czyWygrana=0;
    if (rozmiar==3 || rozmiar==4)
        ile=3;
    else if (rozmiar>4 && rozmiar<=10)
        ile=4;
    else ile=5;

    for (int i=0;i<rozmiar;i++)
    {
        if (czyWygrana==1) break;
        for (int j=0;j<rozmiar;j++)
        {
            if (czyWygrana==1) break;
            czyWyg1=1;
            for ( k=0;k<ile;k++)
                if (j+k<rozmiar)
                    czyWyg1=czyWyg1 && (t[i][j+k]==gracz.znak); // spr poziomo
                else czyWyg1=0;

            if (czyWyg1==1 && k==ile)
            {
                czyWygrana=1;
                break;
            }
            else
                czyWygrana=0;


            czyWyg1=1;
            for ( k=0;k<ile;k++)
            {
                if (i+k<rozmiar)
                    czyWyg1=czyWyg1 && (t[i+k][j]==gracz.znak); // spr pionowo
                else czyWyg1=0;
            }
            if (czyWyg1==1 && k==ile)
            {
                czyWygrana=1;
                break;
            }
            else
                czyWygrana=0;

            czyWyg1=1;
            for ( k=0;k<ile;k++)
            {
                if (i+k<rozmiar)
                    czyWyg1=czyWyg1 && (t[i+k][j+k]==gracz.znak); // spr ukos ->
                else czyWyg1=0;
            }
            if (czyWyg1==1 && k==ile)
            {
                czyWygrana=1;
                break;
            }
            else
                czyWygrana=0;

            czyWyg1=1;
            for ( k=0;k<ile;k++)
            {
                if (i+k<rozmiar)
                    czyWyg1=czyWyg1 && (t[i+k][j-k]==gracz.znak); // spr ukos <-
                else czyWyg1=0;
            }
            if (czyWyg1==1 && k==ile)
            {
                czyWygrana=1;
                break;
            }
            else
                czyWygrana=0;

        }
    }



    if(czyWygrana)
    {
        if (opis==1)
            cout << "\nGRACZ " << gracz.imie << " WYGRYWA!!!\n\n";
        return true;
    }
    return false;
}
//=======================================================================================

