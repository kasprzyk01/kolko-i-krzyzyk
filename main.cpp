#include <QCoreApplication>
#include <iostream>
#include <conio.h>
#include <string>
#include <cstdio>
#include <PlanszaRuch.h>


using namespace std;



//=======================



//========================


//======================
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    SGracz Gracz1, Gracz2;
    SGracz * GraczAktywny=new SGracz();
    int rozmiar;
    int opcjaGry; // 1-player na player, 2- player & komputer
    unsigned char znak;

    do{
        cout<<"Gra w kolko i krzyzyk\n\n\n Wybierz tryb gry:\n";
        cout<<"1 - Gracz kontra gracz\n2 - Gracz kontra komputer\n";

        do{
            cin.clear();
            cin.sync();
            cin>>opcjaGry;
        } while (opcjaGry!=1 && opcjaGry!=2); // dopoki podawane są zle dane

        if (opcjaGry==1)
        {
            cout<<"Podaj imie pierwszego gracza"<<endl;
            cin>>Gracz1.imie;
            cout<<endl<<"Podaj znak dla garcza (X lub O)"<<endl;
            do{
                cin>>Gracz1.znak;
            }while (Gracz1.znak!='X' && Gracz1.znak!='O');

            cout<<"Podaj imie drugiego gracza"<<endl;
            cin>>Gracz2.imie;
            if (Gracz1.znak=='X')
                Gracz2.znak='O';
            else
                Gracz2.znak='X';

            do
            {
                cin.clear();
                cin.sync();
                cout<<"Podaj rozmiar planszy np. 3, 4, 5..."<<endl;
                cin>>rozmiar;
            }
            while(!cin.good()); //dopóki strumień jest w stanie błędu -> dopóki podawane są błędne dane
        }
        else
        {
            cout<<"Podaj imie gracza"<<endl;
            cin>>Gracz1.imie;
            cout<<endl<<"Podaj znak dla garcza (X lub O)"<<endl;
            do{
                cin>>Gracz1.znak;
            }while (Gracz1.znak!='X' && Gracz1.znak!='O');
            rozmiar=3;
            Gracz2.imie="Komputer";
            if (Gracz1.znak='X')
                Gracz2.znak='O';
            else
                Gracz2.znak='X';

        }
        //=========================================

        // tworzenie tablicy gry o podanych rozmiarach
        char** tab = new char*[rozmiar];
        for(int i = 0; i < rozmiar; i++)
            tab[i] = new char[rozmiar];

        for (int i=0; i<rozmiar;i++)
            for(int j=0;j<rozmiar;j++)
                tab[i][j]=' ';

        cout<<"Poczatek gry!! Uwaga !! "<<endl<<endl;
        Rysuj(tab,rozmiar);
        GraczAktywny->imie=Gracz2.imie;
        GraczAktywny->znak=Gracz2.znak;

        if (opcjaGry==1)
        {
            while (1)
            {
                Ruch (tab, rozmiar, &Gracz1);
                if (Wygrana(tab, Gracz1, rozmiar,1) || Remis(tab,rozmiar))
                    break;
                Ruch (tab, rozmiar, &Gracz2);
                if (Wygrana(tab, Gracz2, rozmiar,1) || Remis(tab,rozmiar))
                    break;
            }
        }
        else
        {
            while (1)
            {
                Ruch (tab, rozmiar, &Gracz1);
                if (Wygrana(tab, Gracz1, rozmiar,1) || Remis(tab,rozmiar))
                    break;
                cout<<"\n Ruch komputera:\n";
                minimax(rozmiar, tab, Gracz1, Gracz2, GraczAktywny, 0);
                Rysuj(tab,rozmiar);
                if (Wygrana(tab, Gracz2, rozmiar,1) || Remis(tab,rozmiar))
                    break;
            }

        }

        cout<<"\n Koniec gry!!!\n\n";

        do
        {
        cout<<"Aby wyjsc z gry wybierz klawisz 'n'\nAby zagrac jeszcze raz nacisnij 't' i nacisnij ENTER...\n";
        cin>>znak;
        } while (znak!='n' && znak!='t');
        if( znak == 'n' )
        {

            delete GraczAktywny;
            a.exit();
        }

    } while (znak=='t');

}
/*

*/
