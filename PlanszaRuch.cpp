# include "PlanszaRuch.h"


void Rysuj(char **t, int rozmiar)
{
    cout<<endl;
    for (int i=0;i<rozmiar;i++)
    {
        for (int j=0;j<rozmiar;j++)
        {
            cout << " " << t[i][j] << " ";
            if (j!=rozmiar-1)
                cout<<"|";
        }
        if (i!=rozmiar-1)
        {
            cout << "\n";
            cout << "---+---+---";
            for (int j=0;j<rozmiar-3;j++)
                cout << "+---";
            cout << "\n";
        }
    }
    cout <<endl;
};
//====================================================

void Ruch (char **t, int rozmiar, SGracz *gracz)
{
    int pole, i ,j ;
    do
    {
        cout<<gracz->imie<<", podaj pole (od 1 do "<<rozmiar*rozmiar <<"), w ktore chcesz wstawic "<<gracz->znak<<endl;
        cin>>pole;
        i=(pole-1)/rozmiar;
        j=(pole-1)%rozmiar;
    } while (pole<1 || pole>rozmiar*rozmiar || t[i][j]!=' ');
    t[i][j]=gracz->znak;
    Rysuj(t, rozmiar);
};

int minimax(int rozmiar, char **t, SGracz gracz1, SGracz gracz2,SGracz *aktGracz, int poziom)
{
    int ile = 0;
    int m;
    int maxmin;
    int r,c; // r-row, c - column
    bool czyWygrana;

    //bool czyWygrana=0;


    // sprawdzamy wygraną

    for(int i = 0; i < rozmiar; i++)
        for(int j = 0; j < rozmiar; j++)
            if(t[i][j] == ' ')
            {
                t[i][j] = aktGracz->znak;
                r = i;
                c = j;  // gdyby był remis
                ile++;     // zliczamy wolne pola

                czyWygrana = Wygrana(t, *aktGracz, rozmiar, 0);
                t[i][j] = ' ';
                if(czyWygrana)
                {
                    if(!poziom)
                        t[i][j] = aktGracz->znak;
                    {
                        if (aktGracz->znak==gracz2.znak) // jesli aktywnym graczem jest komputer
                            return -1;
                        else
                            return 1;
                    }

                }
            }

    // sprawdzamy remis

    if(ile == 1)
    {
        if(!poziom)
            t[r][c] = aktGracz->znak;
        return 0;
    }


    // wybieramy najlepszy ruch dla garcza

    if (aktGracz->znak==gracz2.znak)
        maxmin=2;
    else
        maxmin=-2;


    for(int i = 0; i < rozmiar; i++)
        for(int j = 0; j < rozmiar; j++)
            if(t[i][j] == ' ')
            {
                t[i][j] = aktGracz->znak;

                // Rysuj(t,rozmiar);
                // cout<<poziom<<endl;
                if (aktGracz->znak==gracz2.znak)
                    m=minimax(rozmiar, t, gracz1, gracz2, &gracz1, poziom + 1); // wywolujemy z aktywnym graczem - gracz
                else
                    m=minimax(rozmiar, t, gracz1, gracz2, &gracz2, poziom + 1); // wywolujemy z aktywnym graczem - komputerem
                // Rysuj(t,rozmiar);
                t[i][j] = ' ';

                if(((aktGracz->znak == gracz2.znak) && (m < maxmin)) || ((aktGracz->znak == gracz1.znak) && (m >= maxmin)))
                {
                    maxmin = m; r = i; c = j; //zapamietujemy najlepsza pozycje

                }
            }

    if(!poziom)
        t[r][c] = aktGracz->znak; // w najlepsze miejsce wstawiamy znak
    return maxmin;
};
