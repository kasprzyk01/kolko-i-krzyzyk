#-------------------------------------------------
#
# Project created by QtCreator 2014-03-31T21:21:49
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Kolko_krzyzyk
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    PlanszaRuch.cpp \
    WarunkiKonca.cpp


HEADERS += \
    PlanszaRuch.h \
    WarunkiKonca.h

